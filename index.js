const express = require("express");
const app = require("./src/app");

app(express()).listen(7000, () => {
  console.log('App is running at http://localhost:7000')
});