'use strict';


/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('user_game_histories',
    [
      {
        id: 1,
        playedAt: new Date(),
        rank: 1,
        win: 634,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        playedAt: new Date(),
        rank: 3,
        win: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        playedAt: new Date(),
        rank: 2,
        win: 412,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    ) 
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('user_game_histories')
  }
};
